/* user defaults */
#define DEFAULTSIDEBR 0
#define STATUSBARSIZE 20

/* internal defaults */
#define STRBUF 100
#define READBUF 2000

#define CONTROL(ch)   {(ch ^ 0x40)}
#define META(ch)      { 0x1B, ch }

static const Key curskeys[] = {  /*Plain keys here, no CONTROL or META */
    /* keyv.i,                 func,       arg
    { .keyv.i = '\n', f_newline},
*/
    { .keyv.i = KEY_BACKSPACE, f_delete},
/*
    { .keyv.i = KEY_DC,        f_delete,   { .m = m_tosel    } },
    { .keyv.i = KEY_DC,        f_delete,   { .m = m_nextchar } },
    { .keyv.i = KEY_SDC,       f_delete,   { .m = m_tosel } },
    { .keyv.i = KEY_SDC,       f_delete,   { .m = m_nextchar } },
    { .keyv.i = KEY_IC,        f_pipero,   TOCLIP },
    { .keyv.i = KEY_SIC,       f_pipenull, FROMCLIP },
    { .keyv.i = KEY_HOME,      f_moveboth, { .m = m_smartbol } },
    { .keyv.i = KEY_HOME,      f_moveboth, { .m = m_bol      } },
    { .keyv.i = KEY_END,       f_moveboth, { .m = m_eol      } },
    { .keyv.i = KEY_SHOME,     f_moveboth, { .m = m_bof      } },
    { .keyv.i = KEY_SEND,      f_moveboth, { .m = m_eof      } },
    { .keyv.i = KEY_PPAGE,     f_moveboth, { .m = m_prevscr  } },
*/
    { .keyv.i = KEY_NPAGE,     f_pagedown},
    { .keyv.i = KEY_PPAGE,     f_pageup},
    { .keyv.i = KEY_UP,        f_moveup},
    { .keyv.i = KEY_DOWN,      f_movedown},
    { .keyv.i = KEY_LEFT,      f_moveleft},
    { .keyv.i = KEY_RIGHT,     f_moveright},
    { .keyv.i = KEY_RESIZE,    f_resize},
};

static const Key stdkeys[] = {
{ .keyv.c = CONTROL('X'),  f_save},/*
{ .keyv.c = CONTROL('E'),  f_endln},
{ .keyv.c = CONTROL('B'),  f_bgnln},*/
{ .keyv.c = CONTROL('D'),  f_delcopyln},
{ .keyv.c = CONTROL('Y'),  f_copyln},
{ .keyv.c = CONTROL('V'),  f_pasteln},
{ .keyv.c = CONTROL('P'),  f_tabprev},
{ .keyv.c = CONTROL('F'),  f_tabnext},
{ .keyv.c = CONTROL('T'),  f_newtab},
{ .keyv.c = CONTROL('N'),  f_open},
{ .keyv.c = CONTROL('C'),  f_close},
                             };


