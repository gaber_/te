/* See LICENSE for copyright and license details. */
#define NCURSES_WIDECHAR 1
#define _GNU_SOURCE 1

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <locale.h>
#include <wchar.h>
#include <ncursesw/curses.h>
#include <string.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/types.h>

#define LENGTH(x)     ((int)(sizeof (x) / sizeof *(x)))


#define FNAME (file->fname)			/* current file's name */
#define FPREV (file->prev)			/* current file's name */
#define FNEXT (file->next)			/* current file's name */
#define CURRLNNUM (file->current.ln)		/* index number of current line */
#define CURRLN (file->text[CURRLNNUM])	 	/* current line */
#define CURRSTR (CURRLN->c)			/* current line's text string */
#define CURRPOS (file->current.pos)		/* position of cursor on current line's text */
#define LASTLNNUM (file->lines)			/* index number of last line */
#define ALLOCDLINE (file->allocdline)		/* last allocated line pointer */
#define FIRSTWLNNUM (file->firstwln)		/* first line in window */
#define LASTWLNNUM (FIRSTWLNNUM+LINES-2) 	/* last line in window */
#define GETLN(_ln) (file->text[_ln])		/* get line _ln */
#define NOTEXT ALLOCDLINE==0			/* if allocdline==0 the file has no allocated line pointers */

#define MOVEDOWN(_n, _index) _index=_index+_n < LASTLNNUM -1 ? _index+_n : LASTLNNUM -1;

#define MOVEUP(_n, _index) _index=_index-_n > 0 ? _index-_n:0;

#define MOVERIGHT(_n) CURRPOS=CURRPOS+_n < wcslen(CURRSTR) ? CURRPOS+_n : wcslen(CURRSTR);

#define MOVELEFT(_n) CURRPOS=CURRPOS-_n > 0 ? CURRPOS-_n:0;

/* add char to current string in current pos */
#define ADDC(_c) {wmemmove(CURRSTR+CURRPOS+1,CURRSTR+CURRPOS,wcslen(CURRSTR)-CURRPOS+1);\
    CURRSTR[CURRPOS]=_c;\
    CURRPOS++;\
}

/* remove char to current string in current pos */
#define RMC() {CURRPOS=CURRPOS<wcslen(CURRSTR)?CURRPOS:wcslen(CURRSTR);\
    wmemmove(CURRSTR+CURRPOS-1,CURRSTR+CURRPOS,wcslen(CURRSTR)-CURRPOS+1);\
    CURRPOS--;\
}

/* Copy content of src string to dest str */
#define APPENDLINES(_dest, _src) {if (_dest->mul * STRBUF < wcslen(_dest->c) + wcslen(_src->c) + 1) {\
            _dest->mul= (wcslen(_dest->c)+wcslen(_src->c) + 1)/STRBUF+1;\
	    _dest->c=realloc(_dest->c,STRBUF*_dest->mul*sizeof(wchar_t));\
        }\
    wcscat(_dest->c,_src->c);\
}

/* zero file indexes */
#define ZEROFILE() { ALLOCDLINE=0;\
    LASTLNNUM=0;\
    CURRLNNUM=0;\
    CURRPOS=0;\
}

#define INITFILE() { FNAME=NULL;\
    FIRSTWLNNUM=0;\
    ZEROFILE();\
}

typedef struct Line Line;
struct Line {      				/** The internal representation of a line of text */
    wchar_t *c;       				/* Line content */
    size_t height; 				/* Line height */
    size_t mul;				    	/* How many times LINSIZ is c malloc'd to */
    bool dirty;    				/* Should I repaint on screen? */
};

typedef struct {                      		/** A keybinding */
    union {
        char c[6];            			/* Standard chars */
        wchar_t  i;               		/* NCurses code */
    } keyv;
    void (*func)();   				/* Function to perform */
} Key;

struct position {
    int ln;
    int pos;
};

typedef struct Entry Entry;
struct Entry {
    Entry *prev;
    Entry *next;
    wchar_t *fname;
    Line **text;
    int lines;
    int allocdline;                          	/* allocdline - number of lines left free in the array */
    int firstwln;				/* index of first line in window */
    struct position current;
};

char *w_wcstombs(wchar_t *name);

void i_close();
void i_show();
void i_setwindows(int reset);
void i_settopbar();
void i_setstatusbar(wchar_t *status);
void i_add(wchar_t c);
void i_open(wchar_t *fname);
void i_setstatisticsbar();
void i_input(wchar_t **buf, WINDOW *echoto, int *endcondition);

void f_open();
void f_close();
void f_resize();
void f_pageup();
void f_pagedown();
void f_moveup();
void f_movedown();
void f_moveright();
void f_moveleft();
void f_save();
void f_newtab();
void f_tabprev();
void f_tabnext();
void f_delete();
void f_copyln();
void f_delcopyln();
void f_pasteln();

void s_newline();
void s_delline();
void s_initifile();
void s_resetfile();
void s_newentry();

#include "config.h"

Entry *file;	        			/* current file */
wchar_t *topstr=NULL;				/* string to be printed in top bar */
wchar_t *clipboard=NULL;            /* clipboard string container */
WINDOW *topbar;          			/* top bar, contains names of open stuff */
WINDOW *textwindow;       			/* contains document text, or view text */
WINDOW *statusbar;          		/* shows status and errors (if any) */
WINDOW *statisticsbar;         		/* shows cols/rows */
WINDOW *sidebar;          			/* may exist and, if it does, contain numbering */
int sidebarsize=DEFAULTSIDEBR;   		/* 0 if sidebar unset, 1 upwards if set*/
int prevctrl=0;					/* contains previous control char */

/*
 * - Screen handling - *
 */

void f_close() {
    if (prevctrl=='c') i_close();
    prevctrl='c';
    i_setstatusbar(L"Press CTRL_C again to close");
}

void f_resize() {
    clear();
    i_setwindows(1);
}

void f_pagedown() {
    if (!NOTEXT)
        MOVEDOWN(LINES-2, FIRSTWLNNUM);
    CURRLNNUM=FIRSTWLNNUM;
}

void f_pageup() {
    if (!NOTEXT)
        MOVEUP(LINES+2, FIRSTWLNNUM);
    CURRLNNUM=FIRSTWLNNUM;
}

void f_moveup() {
    if (!NOTEXT)
        MOVEUP(1, CURRLNNUM);
}

void f_movedown() {
    if (!NOTEXT)
        MOVEDOWN(1, CURRLNNUM);
}

void f_moveright() {
    if (!NOTEXT)
        MOVERIGHT(1);
}

void f_moveleft() {
    if (!NOTEXT)
        MOVELEFT(1);
}

void f_newtab() {
    s_newentry();
    INITFILE();
}

void f_tabprev() {
    file=FPREV;
}

void f_tabnext() {
    file=FNEXT;
}

void i_adjustwindow() {
    if (CURRLNNUM>=LASTWLNNUM)
        FIRSTWLNNUM=FIRSTWLNNUM+(CURRLNNUM-LASTWLNNUM)+1;
    if (CURRLNNUM<FIRSTWLNNUM)
        FIRSTWLNNUM=CURRLNNUM;
}

void i_show() {
    int i,cury=0,curx=0;
    i_adjustwindow();
    werase(topbar);
    werase(textwindow);
    werase(statisticsbar);
    /* reset statistics bar */
    i_setstatisticsbar();
    /* reset status bar */
    if (prevctrl==0)
        i_setstatusbar(L"");
    /* set top bar */
    i_settopbar();
    waddwstr(topbar,topstr);
    /* set text window */
    i=FIRSTWLNNUM;
    while ((i<LASTWLNNUM)&&(i<LASTLNNUM)) {
        if (CURRLNNUM==i) {
            if (CURRPOS>wcslen(GETLN(i)->c)) {
                waddwstr(textwindow,GETLN(i)->c);
                getyx(textwindow,cury,curx);
            } else {
                waddnwstr(textwindow,GETLN(i)->c,CURRPOS);
                getyx(textwindow,cury,curx);
                waddwstr(textwindow,GETLN(i)->c+CURRPOS);
            }
        } else
            waddwstr(textwindow,GETLN(i)->c);
        waddch(textwindow,'\n');
        i++;
    }
    wmove(textwindow,cury,curx);
    wnoutrefresh(topbar);
    wnoutrefresh(statusbar);
    wnoutrefresh(statisticsbar);
    wnoutrefresh(textwindow);
    doupdate();
}

void i_close() {
    clear();
    endwin();
    exit(0);
}

void i_settopbar() {
    Entry *currfile=file;
    if (topstr!=NULL) free (topstr);

    if (FNAME==NULL) {
        topstr=wcsdup(L"[ new ]");
    }
    else
        topstr=wcsdup(FNAME);

    while (currfile->next != file) {
        currfile=currfile->next;
        if (currfile->fname==NULL) {
            topstr=realloc(topstr,(wcslen(topstr)+13)*sizeof(wchar_t));
            wcscat(topstr,L" | ");
            wcscat(topstr,L"[ new ]");
        }
        else {
            topstr=realloc(topstr,(wcslen(topstr)+wcslen(currfile->fname)+6)*sizeof(wchar_t));
            wcscat(topstr,L" | ");
            wcscat(topstr,currfile->fname);
        }
    }
}

void i_setstatusbar(wchar_t *status) {
    werase(statusbar);
    waddwstr(statusbar,status);
}

void i_setstatisticsbar() {
    wprintw(statisticsbar,"%i,%i of %i,%i",CURRLNNUM,CURRPOS,LASTLNNUM-1,NOTEXT? 0 : wcslen(CURRSTR));
}

void i_setwindows(int reset) {
    if (reset) {
        delwin(topbar);
        delwin(statusbar);
        delwin(textwindow);
    }
    refresh();
    topbar=newwin(1,0,0,sidebarsize);
    textwindow=newwin(LINES-2,0,1,sidebarsize);
    statusbar=newwin(1,COLS-STATUSBARSIZE,LINES-1,sidebarsize);
    statisticsbar=newwin(1,STATUSBARSIZE,LINES-1,COLS-STATUSBARSIZE);
    wattron(topbar, A_REVERSE);
    if (sidebarsize) {
        newwin(0,sidebarsize,0,0);
        wnoutrefresh(sidebar);
    }
    wnoutrefresh(topbar);
    wnoutrefresh(statusbar);
    wnoutrefresh(textwindow);
    doupdate();
}

/*
 * - File handling - *
 */

void i_open(wchar_t *fname) {
    wchar_t c;
    FILE *fp;
    char *fnamestr;
    if (FNAME!=NULL) free (FNAME);
    FNAME=fname;
    fnamestr=w_wcstombs(FNAME);
    if((fp=fopen(fnamestr, "r")) == NULL) {
        free(fnamestr);
        return; /*TODO can't'read file, send a warning */
    }
    free(fnamestr);
    while((c= fgetwc(fp))!=WEOF && errno != EILSEQ)
        i_add(c);
    if (CURRPOS == 0)
        s_delline(CURRLNNUM);
    fclose(fp);
    /* once the file is loaded go back to the beginning of the file */
    CURRLNNUM=0;
    CURRPOS=0;
}

void f_save()
{
    int i;
    FILE *fp;
    char *fnamestr=w_wcstombs(FNAME);
    if (prevctrl!='x') {
        prevctrl='x';
    }
    else i_close();
    if (fnamestr != NULL && ((fp = fopen(fnamestr, "w")) == NULL)) {
        free(fnamestr);
        return; /*TODO can't write on file, send a warning */
    }
    free(fnamestr);

    for (i=0; i<LASTLNNUM; i++) {
        fputws(GETLN(i)->c, fp);
        fputwc(L'\n',fp);
    }
    fclose(fp);
    i_setstatusbar(L"File saved, press CTRL_X again to close.");
}


void f_open() {
    wchar_t *buf;
    int endcondition[]= {'\n','\n',0};
    if (prevctrl!='n') {
        prevctrl='n';
        i_setstatusbar(L"Press CTRL_N again to open new file. Unsaved data will be lost");
        return;
    }
    i_setstatusbar(L"File name: ");
    i_input(&buf,statusbar,endcondition);

    if (!NOTEXT)
        s_resetfile();

    i_open(buf);
}

void f_delete() {
    if (NOTEXT) return;
    if ((CURRPOS > 0)&&(wcslen(CURRSTR)>0))
    {
        RMC();
    }
    else {
        if (CURRLNNUM > 0) {
            CURRPOS=wcslen(GETLN(CURRLNNUM-1)->c);
            APPENDLINES(GETLN(CURRLNNUM-1),CURRLN);
            s_delline(CURRLNNUM);
        }
    }
}

void f_pasteln(){
    if (clipboard == NULL)
        return;
    s_newline();
    CURRSTR=wcsdup(clipboard);
}

void f_copyln(){
    void *tmp;
    if((tmp=wcsdup(CURRSTR))==-1)
        return;
    if (clipboard != NULL)
        free(clipboard);
    clipboard=tmp;
}

void f_delcopyln(){
    f_copyln();
    s_delline(CURRLNNUM);
}

/*
 * - Data processing - *
 */

void i_add(wchar_t c)
{
    if (NOTEXT)
        s_newline();
    else {
        CURRPOS=CURRPOS<wcslen(CURRSTR)?CURRPOS:wcslen(CURRSTR);
        if ((c=='\n')||(c=='\r')) {
            wchar_t *tmp;
            int i=0;
            tmp=wcsdup(&CURRSTR[CURRPOS]);
            CURRSTR[CURRPOS]=L'\0';
            s_newline();
            if (tmp != NULL) {
                while (tmp[i] != L'\0') {
                    i_add(tmp[i]);
                    i++;
                }
                free (tmp);
            }
            CURRPOS=0;
            return;
        }
    }
    /* - NOTE at this point CURRPOS is <= wcslen(CURRSTR) in any case - */

    if(c<32 && c != 9) return;
    else if (wcslen(CURRSTR)+2>=CURRLN->mul*STRBUF)
    {
        CURRLN->mul++;

        CURRSTR=realloc(CURRSTR,STRBUF*CURRLN->mul*sizeof(wchar_t));
    }
    ADDC(c);
}


/*
 * - Misc - *
 */
int keys(wchar_t c)
{
    int i;
    for(i=0; i<LENGTH(curskeys); i++) {
        if (c==curskeys[i].keyv.i) {
            curskeys[i].func();
            return 1;
        }
    }
    for(i=0; i<LENGTH(stdkeys); i++) {
        if(memcmp(&c, stdkeys[i].keyv.c, sizeof c) == 0) {
            stdkeys[i].func();
            return 1;
        }
    }
    return 0;
}

void i_setup() {
    /* Signal handling, default */
    signal(SIGWINCH, f_resize);
    signal(SIGINT,   i_close);
    signal(SIGTERM,  i_close);
    signal(SIGSEGV,  i_close);

    setlocale(LC_CTYPE, "");

    if(!newterm(NULL, stdout, stdin)) {
        newterm("xterm", stdout, stdin);
        i_setstatusbar(L"WARNING! $TERM not recognized!!!");
    }

    noecho();
    raw();
    nl();
    keypad(stdscr, TRUE);

    topstr=NULL;

    s_initifile();

    i_setwindows(0);
}

void i_input(wchar_t **buf, WINDOW *echoto, int *endcondition) {
    int count=0,sz=1,i;
    wint_t c;
    wrefresh(echoto);
    *buf=malloc(sz*sizeof(wchar_t));
    for(;;)
    {
        if (count==sz)
        {
            sz*=2;
            *buf=realloc(*buf,sz*sizeof(wchar_t));
        }
        wget_wch(stdscr,(&c));
        i=0;
        while (endcondition[i]!=0) {
            if (c==endcondition[i]) {
                (*buf)[count]=L'\0';
                return;
            }
            i++;
        }
        waddnwstr(echoto,(wchar_t*)&c,1);
        wrefresh(echoto);
        (*buf)[count]=c;
        count++;
    }
}

/*
 * - Intrnal organization of data -
 */

void s_newline() {
    if (NOTEXT) {
        file->text=malloc(sizeof(Line*));
        LASTLNNUM=1;
        ALLOCDLINE=1;
        CURRLNNUM=0;
    }
    else {
        CURRLNNUM++;
        LASTLNNUM++;

        if (ALLOCDLINE<=LASTLNNUM) {
            ALLOCDLINE*=2;
            file->text=realloc(file->text,sizeof(Line*)*ALLOCDLINE);
        }

        if (CURRLNNUM < LASTLNNUM)
            memmove(&CURRLN+1,&CURRLN,sizeof(Line*)*(LASTLNNUM-CURRLNNUM-1));
    }

    CURRPOS=0;
    CURRLN=malloc(sizeof(Line));
    CURRSTR=malloc(STRBUF*sizeof(wchar_t));
    CURRSTR[CURRPOS]=L'\0';
    CURRLN->mul=1;
}

void s_delline(int ln) {
    if (CURRLNNUM == 0) return;
    free(CURRSTR);
    free(CURRLN);
    if (CURRLNNUM < LASTLNNUM)
        memmove(&CURRLN,&CURRLN+1,sizeof(Line*)*(LASTLNNUM-CURRLNNUM));
    CURRLNNUM--;
    LASTLNNUM--;
}

void s_newentry() {
    Entry *tmp;
    tmp=malloc(sizeof(Entry));
    tmp->next=FNEXT;
    tmp->prev=file;
    FNEXT->prev=tmp;
    FNEXT=tmp;
    file=tmp;
}

void s_resetfile() {
    int i;
    for (i=0; i<LASTLNNUM; i++) {
        free (GETLN(i)->c);
        free (GETLN(i));
    }
    free(file->text);

    ZEROFILE();
}

void s_initifile() {
    file=malloc(sizeof(Entry));
    INITFILE();
    FNEXT=file;
    FPREV=file;
}

/*
 * - wchar->string and vice versa -
 */

char *w_wcstombs(wchar_t *name)
{
    char *target;
    if (name==NULL) return NULL;
    target=malloc(wcslen(name)*8+1);
    wcstombs(target, name, wcslen(name)*8+1);
    return target;
}

wchar_t *w_mbstowcs(char *name)
{
    wchar_t *target;
    if (name==NULL) return NULL;
    target=malloc(strlen(name)*sizeof(wchar_t)+sizeof(wchar_t));
    mbstowcs(target, name, strlen(name)+1);
    return target;
}

/*
 * - main loop etc -
 */

void main_loop() {
    for (; ;)
    {
        wint_t c;
        wget_wch(stdscr,(&c));
        if (!keys(c)) {
            i_add((wchar_t)c);
            prevctrl=0;
        }
        i_show();

        /* process the command keystroke */
    }
}

void usage()
{
    puts("te - the simple editor\n  usage: te [FILE]");
    exit(0);
}


int main(int argc,char **argv)
{
    wchar_t *fname;
    i_setup();
    if(argc>1) {
        fname=w_mbstowcs(argv[1]);
        if (wcscmp(fname,L"-h")==0||wcscmp(fname,L"-help")==0) usage();
        i_open(fname);
    }
    i_show();
    main_loop();
    i_close();
}

